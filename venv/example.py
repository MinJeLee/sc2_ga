class Node(object):             #node의 값과 data값을 비교하여 그 값보다 작거나 같으면 왼쪽, 그렇지 않다면 오른쪽으로 보낸다.
    def __init__(self, data):   #이진트리구조, linked list에서 link_next를 만들듯이 self.left와 self.right를 생성
        self.data = data
        self.left = None
        self.right = None

class BinarySearchTree(object):                                         #재귀구문을 사용하여 Tree를 만들어냄.
    def __init__(self):                                                 #root 생성 및 초기화, 시작구문
        self.root = None

    def insert_value(self, node, data):                                 #node의 값이 없다면 data값을 넣는 동시에 좌, 우측 자식 생성.
        if node is None:
            node = Node(data)
        else:
            if data <= node.data:
                node.left = self.insert_value(node.left, data)          #data의 값이 node의 값과 같거나 그보다 더 적다면 node 좌측 자식로 이동
            else:
                node.right = self.insert_value(node.right, data)        #data의 값이 node의 값보다 크다면 node 우측 자식로 이동
        return node

    def find(self, key):                                                #의문점 : 이구문 필요한 구문인가??
        return self.find_value(self.root, key)

    def find_value(self, root, key):                                    #key값과 같은 값을 찾는 코드  cf) 의문점:왜 이름을 node가 아닌 root로 지었는지..?
        if root is None or root.data == key:                            #key값이 root와 같으면 root 리턴, root = return일 시 미작동
            return root is not None
        elif key < root.data:                                           #key값이 root값보다 작으면 root의 좌측 자식으로 이동
            return self.find_value(root.left, key)
        else:
            return self.find_value(root.right, key)                     #key값이 root값보다 작으면 root의 우측 자식으로 이동

    def delete(self, key):
        self.root, deleted = self.delete_value(self.root, key)
        return deleted

    def delete_value(self, node, key):
        if node is None:
            return node, False                                          #Python에서는 반환값이 2개이상일 수 있다. (튜플형으로 반환)

        deleted = False
        if key == node.data:
            deleted = True
            if node.left and node.right:                                #node값이 key와 같으면서 node가 좌,우측 자식을 다 가지는 경우
                parent, child = node, node.right
                while child.left is not None:                           #node 우측자식의 맨 좌측 아래 값은 node 좌측자식보다 크면서 동시에 node 우측자식보다 작다. = node를 대체할수 있다.
                    parent, child = child, child.left                   #node 우측자식의 맨 좌측 아래 값을 찾기 위한 while문, 그리고 그 해당 노드를 child로 만든다.
                child.left = node.left                                  #node의 좌측자식을 child의 좌측자식으로 연결
                if parent != node:                                      #node의 우측자식이 좌측 자식을 갖지 않는경우를 제외.
                    parent.left = child.right                           #child의 우측자식을 부모의 왼쪽자식 위치로 대체(기존 child의 위치) child라는 변수에 기존값을 가지고있기 때문에 대체가 가능함.
                    child.right = node.right                            #node의 우측자식을 child의 우측자식 위치로 연결
                    node = child                                        #child를 node로 대체
                elif node.left or node.right:                           #node의 자식이 왼쪽or오른쪽만 존재 ->node의 자식을 node로 대체
                    node = node.left or node.right
                else:
                    node = None                                         #node의 자식이 없는경우
            elif key<node.data:                                         #key값이 node값보다 작은경우 ->node 좌측자식의 값을 찾아본다. key값이 더 크면 반대로...
                node.left, deleted = self.delete_value(node.left, key)
            else:
                node.right, deleted = self.delete_value(node.right, key)
            return node, deleted

#여기서부터는 실행구문#

array = [40, 4, 34, 45, 14, 55, 48, 13, 15, 49, 47]

bst = BinarySearchTree()
for x in array:
    bst.insert(x)

# Find
print(bst.find(15)) # True
print(bst.find(17)) # False

# Delete
print(bst.delete(55)) # True
print(bst.delete(14)) # True
print(bst.delete(11)) # False

#출처 :http://ejklike.github.io/2018/01/09/traversing-a-binary-tree-1.html
#거의 주석만 달았습니다.